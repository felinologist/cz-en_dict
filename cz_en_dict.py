# create an empty dictionary
ext_file = open('cz_en.txt', 'r')
cz_en_dict = eval(ext_file.read())
# define the default user input
user_input = 0


# define the update function
def update_dict():
    print()
    print("Enter a Czech word and an English translation.")
    cz_word = input("Czech word: ")
    en_tran = input("English translation: ")
    cz_en_dict.update({cz_word: en_tran})


# define the print function
def print_dict():
    print()
    print("There are the following words in the dictionary:")
    print(cz_en_dict)


# define the find function
def find_word():
    print()
    search_str = input("Enter a Czech word to find: ")
    if search_str in cz_en_dict:
        print("English translation: ", cz_en_dict[search_str])
    else:
        print("No such a word in the dictionary.")


# define the write to file function
def write_to_file():
    dict_file = open('cz_en.txt', 'w')
    dict_file.write(str(cz_en_dict))
    dict_file.close()


# show main menu
while user_input != 4:
    print()
    print("Welcome to the Czech-English dictionary's main menu!")
    print()
    print("1: Add a Czech-English word pair.")
    print("2: Output the dictionary.")
    print("3: Find a Czech word.")
    print("4: Exit.")
    print()
    user_input = input("Select your option: ")

    # option 1: add czech-english word pair
    if user_input == "1":
        update_dict()

    # option 2: list all czech words in the dictionary
    elif user_input == "2":
        print_dict()

    # option 3: look up for a czech word in the dictionary
    elif user_input == "3":
        find_word()

    # option 4: exit
    elif user_input == "4":
        write_to_file()
        break
